package com.example.android.newsapplicationtwo;

import android.text.TextUtils;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by simon on 2018-06-28.
 */

public final class QueryUtils {

    private static final String LOG_TAG = QueryUtils.class.getSimpleName();

    private QueryUtils(){

    }

    public static List<NewsArticle> fetchNewsData(String requestUrl){
        URL url = createUrl(requestUrl);

        String jsonResponse = null;
        try{
            jsonResponse = makeHttpRequest(url);
        }catch (IOException e){
            Log.e(LOG_TAG, "Problem making the HTTP request", e);
        }

        List<NewsArticle> news = extractFeatureFromJson(jsonResponse);

        return news;
    }

    public static List<NewsArticle> extractFeatureFromJson(String newsJSON){
        if (TextUtils.isEmpty(newsJSON)){
            return null;
        }

        List<NewsArticle> news = new ArrayList<>();

        try{

            JSONObject baseJsonResponse = new JSONObject(newsJSON);

            JSONObject response = baseJsonResponse.getJSONObject("response");

            JSONArray newsArray = response.getJSONArray("results");

            String authorName = "";

            for(int i = 0; i < newsArray.length(); i++){

                JSONObject currentNewsArticle = (JSONObject) newsArray.get(i);

                String sectionName = currentNewsArticle.getString("sectionName");

                String webPublicationDate = currentNewsArticle.getString("webPublicationDate");

                String webTitle = currentNewsArticle.getString("webTitle");

                String url = currentNewsArticle.getString("webUrl");

                JSONArray tagsArray = currentNewsArticle.getJSONArray("tags");

                if(tagsArray.length() > 0){
                    for(int j = 0; j<1; j++){
                        JSONObject tags = tagsArray.getJSONObject(j);
                        try{
                            authorName = tags.getString("webTitle");

                        }catch(JSONException e){
                            Log.e(LOG_TAG, "Missing one or more author's name JSONObject");
                        }
                    }
                }
                // Create a new object with
                NewsArticle newsArticle = new NewsArticle(webTitle, sectionName, authorName, webPublicationDate, url);

                // Add the new object to the list of news
                news.add(newsArticle);
            }

        }catch(JSONException e){
            Log.e("QueryUtils", "Problem parsing the articles", e);

        }

        return news;
    }
    private static URL createUrl(String stringUrl){
        URL url = null;
        try{
            url = new URL (stringUrl);
        }catch(MalformedURLException e){
            Log.e(LOG_TAG, "Problem building the URL ", e);
        }
        return url;
    }
    private static String makeHttpRequest(URL url) throws IOException{
        String jsonResponse = "";

        if(url == null){
            return jsonResponse;
        }

        HttpURLConnection urlConnection = null;
        InputStream inputStream = null;
        try{
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(15000);
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            if(urlConnection.getResponseCode() == 200){
                inputStream = urlConnection.getInputStream();
                jsonResponse = readFromStream(inputStream);
            }else{
                Log.e(LOG_TAG, "Error response code: " + urlConnection.getResponseCode());
            }
        }catch (IOException e){
            Log.e(LOG_TAG, "Problem retrieving the news JSON results.", e);
        } finally {
            if(urlConnection != null){
                urlConnection.disconnect();
            }
            if(inputStream != null){
                inputStream.close();
            }
        }
        return jsonResponse;
    }

    private static String readFromStream(InputStream inputStream) throws IOException{
        StringBuilder output = new StringBuilder();
        if(inputStream !=null){
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream, Charset.forName("UTF-8"));
            BufferedReader reader = new BufferedReader(inputStreamReader);
            String line = reader.readLine();
            while(line !=null){
                output.append(line);
                line = reader.readLine();
            }
        }
        return output.toString();
    }

}
