package com.example.android.newsapplicationtwo;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by simon on 2018-06-28.
 */

public class NewsArticleAdapter extends ArrayAdapter<NewsArticle> {

    public NewsArticleAdapter(Context context, List<NewsArticle> news) {
        super(context, 0, news);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View listItemView = convertView;
        if (listItemView == null) {
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        NewsArticle currentNewsArticle = getItem(position);

        String webTitle = currentNewsArticle.getWebtitle();

        TextView webTitleView = (TextView) listItemView.findViewById(R.id.webTitle);

        webTitleView.setText(webTitle);

        String sectionName = currentNewsArticle.getSectionName();

        TextView sectionView = (TextView) listItemView.findViewById(R.id.sectionName);

        sectionView.setText(sectionName);

        String authorName = currentNewsArticle.getAuthorName();

        TextView authorView = (TextView) listItemView.findViewById(R.id.authorName);

        authorView.setText(authorName);

        String publicationDate = currentNewsArticle.getPubdate();

        TextView dateView = (TextView) listItemView.findViewById(R.id.date);

        SimpleDateFormat dateFormat = new SimpleDateFormat("LLL dd, yyyy");

        try {
            Date dateParsed = dateFormat.parse(publicationDate);

            String dateString = dateFormat.format(dateParsed);

            dateView.setText(dateString);
        } catch (ParseException e) {

        }

        return listItemView;
    }
}



